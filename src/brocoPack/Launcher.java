package brocoPack;
import java.io.IOException;

/**
 * Main class for launching BrocoBox
 * @author LeBrun
 *
 */
public class Launcher {

	
	public static void main(String[] args) throws IOException {
		new Broco_generator(System.getProperty("user.home") + "/Desktop").generateBrocoBox();
	}
}
