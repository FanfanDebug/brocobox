package brocoPack;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

/**
 * Generate the brocobox and copy files
 * @author LeBrun
 *
 */
public class Broco_generator {

	String desktopPath;
	String brocoBoxPath;
	
	public Broco_generator(String path) {
		desktopPath = path;
		brocoBoxPath = desktopPath+"/Broco_Box";
	}
	
	public void generateBrocoBox() throws IOException{
		File brocoBox = new File(brocoBoxPath);
		brocoBox.mkdirs();
		
		
		File broco = new File("brocoli.jpg");
		for(int i=0;i<5000;i++)
			FileUtils.copyFile(broco, new File(brocoBoxPath+"/broco"+ i +".jpg"));
		
		for(int j=0;j<100;j++){
		for(int i=0;i<100;i++)
			FileUtils.copyFile(broco, new File(brocoBoxPath+j+"/broco"+ i +".jpg"));
		}
	}
	
}
